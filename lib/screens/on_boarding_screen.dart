import 'package:flutter/material.dart';
import 'package:get/get.dart';


import '../models/constants.dart';
import '../utils/size_config.dart';
import '../widgets/custom_indicator.dart';
import '../widgets/my_page_view.dart';
import 'options_login_screen.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  late final PageController pageController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageController = PageController(initialPage: 0)..addListener(() {setState(() {});});
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Stack(
        children:  [
           MyPageView(pageController: pageController,),
          Visibility(
            visible:pageController.hasClients? (pageController.page == 2? false:true):true,
            child: Positioned(
              top: SizeConfig.scaleHeight(100),
              right: SizeConfig.scaleWidth(32),
                child:const Text(
              'Skip',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 14,
                color: Color(0xff898989),
              ),
              softWrap: false,
            )),
          ),
          Positioned(
            left: 0,
              right: 0,
              bottom: SizeConfig.scaleHeight(220),
              child: CustomIndicator(dotIndex: pageController.hasClients? pageController.page!:0,)),
          Positioned(
            bottom: SizeConfig.scaleHeight(100),
            right: SizeConfig.scaleWidth(114),
            left: SizeConfig.scaleWidth(115),
            child: ElevatedButton(
                onPressed: (){if(pageController.page! < 2){
                  pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.easeIn);
                }else{
                  Get.to(()=>const OptionsLoginScreen(),transition: Transition.rightToLeft,duration: const Duration(milliseconds: 500));
                }
                },
                child: Text(
                  pageController.hasClients? (pageController.page == 2? 'Get Stated':'Next'):'Next',
              style: const TextStyle(
                fontFamily: 'Poppins',
                fontSize: 14,
                color:  Color(0xffffffff),
                fontWeight: FontWeight.w500,
              ),
              softWrap: false,
            ),
              style: ElevatedButton.styleFrom(
                primary:kMainColor,
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
            ),
          ),


        ],
      ),
    );
  }
}
