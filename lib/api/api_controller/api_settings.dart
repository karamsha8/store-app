class ApiSettings{
  static const apiBaseUrl ='https://student.valuxapps.com/api/';
  static const userLogin = apiBaseUrl+'login';
  static const homeData = apiBaseUrl+'home';
  static const userLogout = apiBaseUrl+'logout';
}