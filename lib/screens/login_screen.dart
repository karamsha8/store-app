
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';


import '../api/api_controller/use_api_controller.dart';
import '../models/api_models/user.dart';
import '../storage/data_share_pref_controller.dart';
import '../utils/helpers.dart';
import '../widgets/app_text_field.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;

  late TapGestureRecognizer _tapGestureRecognizer;

  @override
  void initState() {
    super.initState();
    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();

    _tapGestureRecognizer = TapGestureRecognizer()
      ..onTap = navigateToForgetPassword;
  }

  void navigateToForgetPassword() =>
      Navigator.pushNamed(context, '/forget_password_screen');

  @override
  void dispose() {
    _emailTextEditingController.dispose();
    _passwordTextEditingController.dispose();
    _tapGestureRecognizer.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue.shade900,
            Colors.blue.shade200,
          ],
          begin: AlignmentDirectional.topEnd,
          end: AlignmentDirectional.bottomStart,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text(
            'LOGIN',
            style:  TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body:  ListView(
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
          children:  [
            const Text(
              'Welcome back...',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            const Text(
              'Enter your email & password ',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
            const  SizedBox(height: 30),
            AppTextField(
              hint: 'Email',
              prefixIcon: Icons.email,
              textEditingController: _emailTextEditingController,
            ),
            const SizedBox(height: 10),
            AppTextField(
              hint: 'Password',
              prefixIcon: Icons.lock,
              obscure: true,
              textEditingController: _passwordTextEditingController,
            ),
            const SizedBox(height: 10),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'ForgetPassword?',
                children: [
                  const  TextSpan(text: ' '),
                  TextSpan(
                    text: 'Reset Now',
                    recognizer: _tapGestureRecognizer,
                  ),
                ],
              ),
            ),
            const  SizedBox(height: 20),
            ElevatedButton(
              onPressed: () async {
                await performLogin();
                print('on pressed success');
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                minimumSize:const Size(double.infinity, 50),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              child: const Text(
                'LOGIN',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ) ],
        ),
      ),
    );
  }
  Future performLogin() async{
    if(checkData()){
      await login();
    }
  }
  bool checkData(){
    if(_emailTextEditingController.text.isNotEmpty && _passwordTextEditingController.text.isNotEmpty){
      return true;
    }
    Helpers.showSnackBar(context: context, message: 'Enter email & password',
      error: true,);
    return false;
  }
  Future login() async{
    UserData? userData = await UserApiController().login(context,_emailTextEditingController.text, _passwordTextEditingController.text);
    print('login in login success');
    if(userData != null){
      DataSharedPrefController().save(userData);
      Helpers.showSnackBar(context: context, message: 'Logged in successfully');
      Future.delayed(const Duration(seconds: 2),(){
        Navigator.pushReplacementNamed(context, '/main_screen');

      });
    }
  }
}

/*
* import 'package:flutter/material.dart';
import 'package:my_shop_app/utils/custom_buttons.dart';
import 'package:my_shop_app/utils/size_config.dart';

import '../utils/custom_text_field.dart';
class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: SizeConfig.scaleHeight(85),),
            const Text(
              'Enter Your Name',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16,
                color:  Color(0xff0c0b0b),
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15),),
            CustomTextFormField(),
            SizedBox(height: SizeConfig.scaleHeight(34),),

            const Text(
              'Enter Your Phone Number',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16,
                color:  Color(0xff0c0b0b),
                fontWeight: FontWeight.w600,
                height: 1.5625,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15),),
            const CustomTextFormField(),
            SizedBox(height: SizeConfig.scaleHeight(30),),
            const  Text(
              'Add Address',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 16,
                color:  Color(0xff0c0b0b),
                fontWeight: FontWeight.w600,
                height: 1.5625,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15),),
            const CustomTextFormField(maxLines: 5,),
            SizedBox(height: SizeConfig.scaleHeight(50),),
            const CustomGeneralButton(text: 'Login',),
          ],
        ),
      ),
    );
  }
}
*/