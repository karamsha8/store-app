import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import 'login_screen.dart';


class OptionsLoginScreen extends StatelessWidget {
  const OptionsLoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            // height: SizeConfig.scaleHeight(119),
            height: 119,
          ),
          Image.asset(
            'images/store6.jpg',
            // height: SizeConfig.scaleHeight(300),
            height: 119,
          ),
           Text(
            'Store App',
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 51,
              color: Colors.blue.shade300,
            ),
          ),
          SizedBox(
            // height: SizeConfig.scaleHeight(119),
            height: 119,
          ),
          Row(
            children: [
              Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() => const LoginScreen(),
                          transition: Transition.rightToLeft,
                          duration: const Duration(milliseconds: 500));
                    },
                    child: Container(
                      // height: SizeConfig.scaleHeight(45),
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                              width: 1.0, color: const Color(0xff707070))),
                      child: Row(
                        children: [
                          SizedBox(
                            // width: SizeConfig.scaleWidth(16.0),
                            width: 16,
                          ),
                          const Icon(
                            Icons.login_outlined,
                            color: Color(0xffF14336),
                          ),
                          SizedBox(
                            // width: SizeConfig.scaleWidth(17.4),
                            width: 16,
                          ),
                          const Text(
                            'LOGIN',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 14,
                              color: Color(0xff000000),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      // height: SizeConfig.scaleHeight(45),
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                              width: 1.0, color: const Color(0xff707070))),
                      child: Row(
                        children: [
                          SizedBox(
                            // width: SizeConfig.scaleWidth(16.0),
                            width: 16,
                          ),
                          const Icon(
                            FontAwesomeIcons.googlePlusG,
                            color: Color(0xff3B5998),
                          ),
                          SizedBox(
                            // width: SizeConfig.scaleWidth(17.4),
                            width: 16,
                          ),
                          const Text(
                            'Sign in with ',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 14,
                              color: Color(0xff000000),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
