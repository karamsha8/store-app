import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

import '../models/constants.dart';

class CustomIndicator extends StatelessWidget {
   CustomIndicator({Key? key,required this.dotIndex}) : super(key: key);
late final double dotIndex;
  @override
  Widget build(BuildContext context) {
    return DotsIndicator(
      decorator:  DotsDecorator(
        activeColor: kMainColor,
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: const BorderSide(color: kMainColor),
        )
      ),
      dotsCount: 3,
      position: dotIndex,
    );
  }
}
