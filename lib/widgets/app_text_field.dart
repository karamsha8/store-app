import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  final TextInputType textInputType;
  final String hint;
  final bool obscure;
  final IconData prefixIcon;
  final TextEditingController textEditingController;

  AppTextField({
    this.textInputType = TextInputType.text,
    required this.hint,
    this.obscure = false,
    required this.textEditingController,
    required this.prefixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: obscure,
      keyboardType: textInputType,
      controller: textEditingController,
      style: const TextStyle(color: Colors.white),
      decoration: InputDecoration(
        hintText: hint,
        hintStyle:const TextStyle(color: Colors.white70),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide:const BorderSide(
            width: 1,
            color: Colors.white70,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide:const BorderSide(
            width: 1,
            color: Colors.white,
          ),
        ),
        prefixIcon: Icon(prefixIcon, color: Colors.white70),
        // suffix: Icon(Icons.check),
      ),
    );
  }
}
