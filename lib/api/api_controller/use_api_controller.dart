import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;


import '../../models/api_models/user.dart';
import '../../storage/data_share_pref_controller.dart';
import '../../utils/helpers.dart';
import 'api_settings.dart';

class UserApiController{
  Future<UserData?> login (BuildContext context,String email,String password)async{
    print('LOGIN URL: ${ApiSettings.userLogin}');
    var url = Uri.parse(ApiSettings.userLogin);
    var response = await http.post(url,body:{
      'email':email,
      'password':password,
    });
    if(response.statusCode == 200){
      var userDataJsonObject = jsonDecode(response.body)['data'];
      print('Login Success');
      return UserData.fromJson(userDataJsonObject);
    }else{
      var jsonResponse = jsonDecode(response.body);
      print('Login field');
      Helpers.showSnackBar(context: context, message: jsonResponse['message'],error: true);
    }
    return null;
  }

  Future<bool> logout()async{
    var url = Uri.parse(ApiSettings.userLogout);
    var response = await http.post(url,headers: {
      HttpHeaders.authorizationHeader:DataSharedPrefController().getToken(),
    });
    print('logout 1');
    if(response.statusCode == 200){
      print('logout 1');
      DataSharedPrefController().logout();
      print('logout 2');
      return true;
    }
    return false;
  }
}