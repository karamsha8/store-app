import 'package:shared_preferences/shared_preferences.dart';

import '../models/api_models/user.dart';

class DataSharedPrefController{
  static final DataSharedPrefController _instance= DataSharedPrefController._internal();
  late final SharedPreferences _sharedPreferences;
  factory DataSharedPrefController(){
    return _instance;
  }
  DataSharedPrefController._internal();

  Future<void> initSharedPref() async{
    _sharedPreferences = await SharedPreferences.getInstance();
  }
  Future save(UserData userData) async{
    await _sharedPreferences.setBool('logged_in', true);
    await _sharedPreferences.setInt('id', userData.id);
    await _sharedPreferences.setString('name', userData.name);
    await _sharedPreferences.setString('email', userData.email);
    await _sharedPreferences.setString('token',userData.token);
  }

  bool isLoggedIn(){
    return _sharedPreferences.getBool('logged_in') ?? false;
  }

  String getToken(){
    String token = _sharedPreferences.getString('token')??'';
    return 'Bearer $token ';
  }

  Future<bool> logout() async{
    return await _sharedPreferences.clear();
  }

}