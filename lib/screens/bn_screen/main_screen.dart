import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/screens/bn_screen/product_screen.dart';
import 'package:store_app/screens/bn_screen/settings_screen.dart';

import '../../api/api_controller/use_api_controller.dart';
import '../../models/bn_model.dart';
import '../../utils/helpers.dart';
import '../search_screen.dart';
import 'categories_screen.dart';
import 'favorites_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedItem = 0;

  final List<BnScreen> _screen = <BnScreen>[
    BnScreen(widget: const ProductScreen(), title: 'Product'),
    BnScreen(widget:  CategoriesScreen(), title: 'Categories'),
    BnScreen(widget: const FavoritesScreen(), title: 'Favorites'),
    BnScreen(widget: const SettingScreen(), title: 'Settings'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_screen.elementAt(_selectedItem).title),
        actions: [
          IconButton(
            onPressed: () {
              Get.to(const SearchScreen());
            },
            icon: const Icon(Icons.search),
          ),
          IconButton(
            onPressed: () async{
              await logout();
            },
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedItem,
        onTap: (int selectedItem) {
          setState(() {
            _selectedItem = selectedItem;
          });
        },
        elevation: 4,
        unselectedIconTheme: IconThemeData(
          color: Colors.grey.shade400,
        ),
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.category), label: 'Category'),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite), label: ' Favorite'),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), label: ' Settings'),
        ],
      ),
      body: _screen.elementAt(_selectedItem).widget,
    );
  }
  Future logout() async{
    bool loggedOut = await UserApiController().logout();
    if(loggedOut){
      Navigator.pushNamedAndRemoveUntil(context, '/options_login_screen', (route) => false);
    }else{
      Helpers.showSnackBar(context: context, message: 'Logout failed',error: true);
    }
  }

}
