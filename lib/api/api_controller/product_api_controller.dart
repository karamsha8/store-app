
import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../models/api_models/home_data.dart';
import '../../models/api_models/productModel.dart';

class ProductsApiController{

  Future<List<ProductModel>> getTasks() async {
    List<ProductModel> tasks = [];
    var url = Uri.parse('https://fakestoreapi.com/products');
    var response = await http.get(url);

    if (response.statusCode != 400 || response.statusCode != 500) {
      var tasksJsonList = jsonDecode(response.body) as List;
      tasks = tasksJsonList
          .map((taskObjectJson) => ProductModel.fromJson(taskObjectJson))
          .toList();
    }
    return tasks;
  }
}
// Future<List<Products>> getHomeData() async {
//   var url = Uri.parse('https://fakestoreapi.com/products');
//   var response = await http.get(url, );
//   if (response.statusCode != 400 || response.statusCode != 500) {
//     print('we we');
//       List<dynamic> data =jsonDecode(response.body);
//       List<Products> productList=[];
//       print('Message: We Are after homeModelData');
//       for(int i=0; i<data.length;i++){
//         productList.add(Products.fromJson(data[i]),
//         );
//       }
//       return productList;
//     }
//   return [];
// }


