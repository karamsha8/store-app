import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../api/api_controller/home_api_controller.dart';
import '../../models/api_models/home_data.dart';
import '../../models/api_models/productModel.dart';
import '../../widgets/custom_card.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  late Future<List<MyHomeData?>> _myHomeDataFuture;
  List<MyHomeData?> _myHomeDateVariable = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _myHomeDataFuture = HomeApiController().getHomeData();

  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => getHomeDataFuture(),
      child: FutureBuilder<List<MyHomeData?>>(
        future: _myHomeDataFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            print("karam = snapshot has data");
            _myHomeDateVariable = snapshot.data ?? [];
            return ListView.builder(
              itemCount: _myHomeDateVariable.length,
              itemBuilder: (context, index) {
                MyHomeData? myHomeDataColumn =
                _myHomeDateVariable.elementAt(index);

                return CarouselSlider(

                  items: myHomeDataColumn!.banners!
                      .map((e)
                  =>
                      Image(image: NetworkImage(e.image!),
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),


                  )
                      .toList(),

                  options: CarouselOptions(
                    height: 250,
                    initialPage: 0,
                    reverse: false,
                    autoPlay: true,
                    autoPlayAnimationDuration: const Duration(seconds: 1),
                    autoPlayInterval: const Duration(seconds: 1),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    scrollDirection: Axis.horizontal,
                    enableInfiniteScroll: true,
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            print('Error = ${snapshot.error}');
            print('Data = ${snapshot.data}');
            return Center(
              child: Column(
                children: const [
                  Icon(
                    Icons.warning,
                    size: 70,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'NO DATA',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: Column(
                children: [
                  const Icon(
                    Icons.warning,
                    size: 70,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'NO DATA',
                    style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 20,
                    ),
                  )
                ],
              ),
            );
          }
        },
      ),
    );
  }

  Future getHomeDataFuture() async {
    _myHomeDateVariable.clear();
    _myHomeDataFuture = HomeApiController().getHomeData();
  }
}


