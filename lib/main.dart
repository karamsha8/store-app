import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:store_app/screens/bn_screen/main_screen.dart';
import 'package:store_app/screens/launch_screen.dart';
import 'package:store_app/screens/options_login_screen.dart';
import 'package:store_app/storage/data_share_pref_controller.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await DataSharedPrefController().initSharedPref();
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/options_login_screen',
      routes: {
        '/launch_screen':(context) => const LaunchScreen(),
        '/main_screen':(context) => const MainScreen(),
        '/options_login_screen':(context) => const OptionsLoginScreen(),
      },
    );
  }
}
