class ProductModel {
  dynamic id;
  String? title;
  dynamic price;
  String? description;
  String? category;
  String? image;
  Rating? rating;



  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    price = json['price'];
    description = json['description'];
    category = json['category'];
    image = json['image'];
    rating =
    json['rating'] != null ?  Rating.fromJson(json['rating']) : null;
  }


}

class Rating {
  dynamic rate;
  dynamic count;

  Rating.fromJson(Map<String, dynamic> json) {
    rate = json['rate'];
    count = json['count'];
  }


}