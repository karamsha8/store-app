import 'package:flutter/material.dart';
import '../models/constants.dart';
import '../storage/data_share_pref_controller.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  State<LaunchScreen> createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen>
    with SingleTickerProviderStateMixin {
  // late AnimationController animationController;
  // late Animation<double> fadingAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // animationController =
    //     AnimationController(vsync: this, duration: const Duration(seconds: 1));
    //
    // fadingAnimation =
    //     Tween<double>(begin: .2, end: 1).animate(animationController);
    // animationController.repeat(reverse: true);
    Future.delayed( const Duration(seconds: 3),(){
      String route = DataSharedPrefController().isLoggedIn() ? '/main_screen' : '/options_login_screen';
      Navigator.pushReplacementNamed(context, route,);
    });
  }
  @override
  // void dispose() {
  //   // TODO: implement dispose
  //   super.dispose();
  //   animationController.dispose();
  //
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:

      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // const Spacer(),
          // const Text(
          //   'Store App ',
          //   style: TextStyle(
          //     fontFamily: 'Poppins',
          //     fontWeight: FontWeight.bold,
          //     fontSize: 51,
          //     color: Color(0xffffffff),
          //   ),
          // ),
          Image.asset('images/store7.png',height: double.infinity,),
        ],
      ),
    );
  }
}
