import 'dart:convert';
import 'dart:io';


import 'package:http/http.dart' as http;

import '../../models/api_models/base_response.dart';
import '../../models/api_models/home_data.dart';
import '../../storage/data_share_pref_controller.dart';
import 'api_settings.dart';

class HomeApiController {
  // Future<List<MyHomeData>> getHomeData() async {
  //   var url = Uri.parse(ApiSettings.homeData);
  //   var response = await http.get(url, headers: {
  //     'lang': 'ar',
  //     'Content-Type': 'application/json',
  //      HttpHeaders.authorizationHeader: DataSharedPrefController().getToken(),
  //   });
  //   if (response.statusCode != 400 || response.statusCode != 500) {
  //     print('we we');
  //       HomeModelData homeModelData =
  //           HomeModelData.fromJson(jsonDecode(response.body));
  //       print('Message: We Are after homeModelData');
  //       return homeModelData.homeData;
  //     }
  //   return [];
  // }

  Future<List<MyHomeData>> getHomeData() async {
    var url = Uri.parse(ApiSettings.homeData);
    var response = await http.get(url, headers: {
      'lang': 'ar',
      'Content-Type': 'application/json',
       HttpHeaders.authorizationHeader: DataSharedPrefController().getToken(),
    });

    if (response.statusCode != 400 || response.statusCode != 500) {
    print('we we');
    HomeModelData homeModelData = HomeModelData.fromJson(jsonDecode(response.body));
    return homeModelData.homeData;
    }
  return [];
  }

// Future<List<Products>> getHomeData() async {
//   var url = Uri.parse('https://fakestoreapi.com/products');
//   var response = await http.get(url, );
//   if (response.statusCode != 400 || response.statusCode != 500) {
//     print('we we');
//       List<dynamic> data =jsonDecode(response.body);
//       List<Products> productList=[];
//       print('Message: We Are after homeModelData');
//       for(int i=0; i<data.length;i++){
//         productList.add(Products.fromJson(data[i]),
//         );
//       }
//       return productList;
//     }
//   return [];
// }


//
// Future<List<MyHomeData>> getHomeData() async {
//   var url = Uri.parse(ApiSettings.homeData);
//   var response = await http.get(url);
//   if (response.statusCode != 400 || response.statusCode != 500) {
//     var jsonResponse = jsonDecode(response.body);
//     var usersJsonArray = jsonResponse['data'] as List;
//     return usersJsonArray.map((userJsonObject) => MyHomeData.fromJson(userJsonObject)).toList();
//   }
//   return [];
// }
}
