

import 'package:store_app/models/api_models/user.dart';

class BaseResponse {
  late bool status;
  late String? message;
  UserData? data;


  BaseResponse.fromJson(json) {
    status = json['status'];
    print('$status');
    message = json['message'];
    print('$message');


     data = json['data'] != null ? UserData.fromJson(json['data']) : null;




  }
}
