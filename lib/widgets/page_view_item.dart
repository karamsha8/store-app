import 'package:flutter/material.dart';

import '../utils/size_config.dart';

class PageViewItem extends StatelessWidget {
  late final String image;
  late final String title;
  late final String subtitle;

  PageViewItem({
   required this.image,
   required this.title,
   required this.subtitle,
  } );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.scaleHeight(180),),
        SizedBox(child:Image.asset(image),),
          SizedBox(height: SizeConfig.scaleHeight(48),),
         Text(
          title,
          style:const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 20,
            color: Color(0xff2f2e41),
            fontWeight: FontWeight.w600,
          ),
          softWrap: false,
        ),
         SizedBox(height: SizeConfig.scaleHeight(16),),
         Text(
          subtitle,
          style:const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 15,
            color: Color(0xff78787c),
          ),
          softWrap: false,
        ),
      ],
    );
  }
}
